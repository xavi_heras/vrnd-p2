﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class Fade : MonoBehaviour {

	public Image i;
	public float _colorSpeed = 0.005f;
	private Color c;
	private bool start = false;

	// Update is called once per frame
	void Update () {
		if (start) {
			if (c.a > 0.0f)
				c.a = c.a - _colorSpeed;
			i.color = c;
		}	
	}

	public void StartFade (){
		c = i.color;
		c.a = 1.0f;
		start = true;
	}
}
