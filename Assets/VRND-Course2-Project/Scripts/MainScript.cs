﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainScript : MonoBehaviour {


	public int sceneToLoad;
	public bool onButton = false;
	public bool loading = false;

	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {	

		if (GvrViewer.Instance.Triggered) {
			if (onButton)
				fadeToLoad ();
		} 

		if (loading && GetComponent<FadeAndLoadScene>().fadeComplete){
			SceneManager.LoadScene (sceneToLoad);
		}

	}

	public void pointerEnter (){
		onButton = true;
	}

	public void pointerExit (){
		onButton = false;
	}

	void fadeToLoad(){
		if (!loading) {
			loading = true;
			GetComponent<FadeAndLoadScene> ().StartFade ();
		} 
	}
}
