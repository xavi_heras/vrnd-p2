﻿using UnityEngine;
using System.Collections;

//Scrip to control animation speed. Stops and resumes animation.
public class TriggerAnimation : MonoBehaviour {
    
    public Animator stateMachine;

    private bool created = false;

    void Awake() {
        if (GvrViewer.Instance == null) {
            GvrViewer.Create();
            created = true;
        }
    }

    void Start() {
        if (created) {
            foreach (Camera c in GvrViewer.Instance.GetComponentsInChildren<Camera>()) {
                c.enabled = false; // to use the Gvr SDK without adding cameras we have to disable them
            }
        }

    }

    void Update() {
        GvrViewer.Instance.UpdateState(); //need to update the data here otherwise we dont get mouse clicks; this is because we are automatically creating the GVRSDK (seems like a bug)
		stateMachine.speed = stateMachine.GetFloat("myRotateGlobeSpeed");
		if (GvrViewer.Instance.Triggered) {
			stateMachine.SetFloat ("myRotateGlobeSpeed", stateMachine.GetFloat ("myRotateGlobeSpeed") == 0 ? 1 : 0);
		}
    }

}