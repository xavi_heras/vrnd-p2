﻿using UnityEngine;
using System.Collections;

public class Waypoint : MonoBehaviour
{
	private enum State
	{
		Idle,
		Focused,
		Clicked
	}

	[SerializeField]
	private State  		_state					= State.Idle;
	private AudioSource _audio_source			= null;
	private float 		_endFocusTime			= 0f;

	[Header("Sounds")]
	public AudioClip clip_click					= null;				

	[Header("Auto click time")]
	public float time_to_autoclick				= 2f;

	void Awake()
	{		
		_audio_source				= gameObject.GetComponent<AudioSource>();	
		_audio_source.clip		 	= clip_click;
		_audio_source.playOnAwake 	= false;
	}


	void Update()
	{		
		switch(_state)
		{
			case State.Focused:
				Focus();
				break;
			default:
				break;
		}
			
	}


	public void Enter()
	{
		_state = _state == State.Idle ? State.Focused : _state;
		_endFocusTime = Time.unscaledTime + time_to_autoclick;
	}


	public void Exit()
	{
		_state = State.Idle;
		_endFocusTime = 0f;
	}
		
	public void Click()
	{
		_state = _state == State.Focused ? State.Clicked : _state;
		
		_audio_source.Play();

		Camera.main.transform.position 	= gameObject.transform.position;
	}
		
	public void Focus()
	{		
		if (Time.unscaledTime >= _endFocusTime)
			Click ();
	}


}
