﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FadeAndLoadScene : MonoBehaviour {

	public Image i;
	public float _colorSpeed = 0.005f;
	private Color c;
	private bool start = false;
	public bool fadeComplete = false;

	// Update is called once per frame
	void Update () {
		if (start) {
			if (c.a < 1.0f)
				c.a = c.a + _colorSpeed;
			i.color = c;
		}

		if (c.a >= 1f)
			fadeComplete = true;
	}

	public void StartFade (){		
		c = i.color;
		c.a = 0.0f;
		start = true;
		fadeComplete = false;
	}
}
