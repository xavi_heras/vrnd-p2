﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Follow target position. Used to make stars follow user.
public class FollowPosition : MonoBehaviour {

	public Transform target;

	public bool followX;
	public bool followY;
	public bool followZ;

	private Vector3 startPosition;

	// Use this for initialization
	void Start () {
		startPosition = transform.position;	

	}
	
	// Update is called once per frame
	void Update () {
		this.transform.position = new Vector3(followX ? target.position.x : startPosition.x,
											  followY ? target.position.y : startPosition.y,
											  followZ ? target.position.z : startPosition.z);
	}
}
