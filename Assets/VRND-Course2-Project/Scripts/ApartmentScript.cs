﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ApartmentScript : MonoBehaviour {

	public GameObject fadePanel;
	public AudioSource doorSound;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {		
		if (doorSound != null) {
			if (!doorSound.isPlaying) {				
				Destroy (doorSound);	
				fadePanel.GetComponent<Fade> ().StartFade ();
			}
		}

	}
}
