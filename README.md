Build an apartment by Xavi Heras

Unity version: 5.5.1f1
Platform: Android
Test device: Pixel XL


I really enjoyed this project a lot. From the begining my objective was to create a realistic scene. A relaxed evening atmosphere from a very high skyscraper with huge windows to enjoy a beautiful city skyline and provoke a feeling of vertigo. The provided apartment layout was not exactly what i needed so used some free asset store plugins to model the new walls, floor and add some objects (lamps, kitchen utensils, pictures, etc). Included features:

	- Main scene (reticle click tutorial)
	- Navigation system with gaze or click input (focus 3 seconds to teleport to the waypoint)
	- Positional sounds (city ambience, refrigerator)
	- Particles (navigation points and stars)

At the end took me around 30 hours to finish this project. Most of the time was used to place all models around the scene and configure/bake lights. 

What I liked: Add every little detail to increase the feeling of presence was so much fun. 

What was challenging: Stop adding more detail or new features! I underestimated a lot how diferent is to design experiences in VR. Level design, scale, interactions, user experience... all was an amazing challenge.


Video walkthrough: https://www.youtube.com/watch?v=HOnegqfrKyo
